#version 150 core

uniform sampler2D diffuseTex;//scene
uniform sampler2D bloomTex;

in Vertex	{
  vec2 texCoord;
	vec4 colour;
} IN;

out vec4 gl_FragColor;

void main(void){
  vec4 bloom = texture(bloomTex, IN.texCoord);
  gl_FragColor = texture(diffuseTex, IN.texCoord) + bloom;
}
