#version 150 core

uniform int rand;
uniform float completion;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projMatrix;

layout(lines) in;
layout(line_strip, max_vertices = 10) out;

out Vertex{
  vec4 colour;
} OUT;

int nextRand(int r){
  return r * 23432537;
}

void main()
{
  int r = rand;
  //uses the fraction of completion because this is effectively a random number
  //and it changes every draw call unlike rand
  OUT.colour = vec4(0.7,fract(completion) , 0.9, 1.0);

  gl_Position = gl_in[1].gl_Position;
  EmitVertex();

  //between 5 and 8
  int num = (r%4) + 5;
  vec4 dir = gl_in[0].gl_Position - gl_in[1].gl_Position;
  dir /= (num + 1);
  vec4 current = gl_in[1].gl_Position;

  for(int i = 0; i < num; i++){
    if(i > float(num)*completion/150.0){
      return;
    }
    current += dir;
    int r1 = nextRand(r);
    int r2 = nextRand(r1);
    r = nextRand(r2);
    //TODO make an average between every two zigzags and move it slightly
    //(and double make output)
    gl_Position = current + ((projMatrix * viewMatrix * modelMatrix)*vec4(float(r1%10/30.0) - 0.2,0.0,float(i%10/30.0) - 0.2,0.0));
    EmitVertex();
  }

  gl_Position = gl_in[0].gl_Position;
  EmitVertex();


  EndPrimitive();
}
