#version 330 core

uniform sampler2D diffuseTex;
uniform sampler2D bumpTex;

uniform vec3 cameraPos;
uniform vec4 lightColour[5];
uniform vec3 lightPos[5];
uniform float lightRadius[5];
uniform int lightOn[5];

in Vertex{
  vec4 colour;
  vec2 texCoord;
  vec3 normal;
  vec3 tangent;
  vec3 binormal;
  vec3 worldPos;
} IN;

out vec4 gl_FragColor;


void main(void){
  mat3 TBN = mat3(IN.tangent, IN.binormal, IN.normal);

  vec4 diffuse = texture(diffuseTex, IN.texCoord);
  vec3 normal = normalize(TBN * (texture(bumpTex, IN.texCoord).rgb * 2.0 - 1.0));

  gl_FragColor = vec4(0,0,0,0);
  for(int i = 0; i < 5; i++){
    vec3 incident = normalize(lightPos[i] - IN.worldPos);
    float lambert = max(0.0, dot(incident, normal));

    float dist = length(lightPos[i] - IN.worldPos);
    float atten = 1.0 - clamp(dist/lightRadius[i],0.0,1.0);

    vec3 viewDir = normalize(cameraPos - IN.worldPos);
    vec3 halfDir = normalize(incident + viewDir);

    float rFactor = max(0.0, dot(halfDir, normal));
    float sFactor = pow(rFactor, 50.0);

    vec3 colour = (diffuse.rgb * lightColour[i].rgb);
    colour += (lightColour[i].rgb * sFactor) * 0.33;

    gl_FragColor += vec4(colour*atten*lambert, diffuse.a) * 1.5;
    gl_FragColor.rgb += (diffuse.rgb * lightColour[i].rgb) * 0.1;
  }

  gl_FragColor /= 5;
  //make it look like the lava is glowing
  gl_FragColor += vec4(1,0.3,0,1) * clamp((700-IN.worldPos.y)/700.0,0,1);
}
