#version 150 core
//drunk vision texture
uniform sampler2D diffuseTex;

uniform int time;

uniform float PI = 3.14159265359;

in Vertex{
  vec2 texCoord;
  vec4 colour;
} IN;

out vec4 gl_FragColor;

void main(void){
  vec4 original = texture(diffuseTex, IN.texCoord);
  //uses prime numbers so the kinds of vision rarely are looking in the same place
  vec2 offset1 = vec2(sin(float(time%2200*PI/1100.0))/10,sin(float(time%1400*PI/700.0))/30);
  vec2 offset2 = vec2(sin(float(time%2600*PI/1300.0))/10,sin(float(time%3400*PI/1700.0))/30);
  vec2 secondPos1;
  secondPos1.x = IN.texCoord.x + (sin(PI * IN.texCoord.x) * offset1.x);
  secondPos1.y = IN.texCoord.y + (sin(PI * IN.texCoord.y) * offset1.y);
  vec2 secondPos2;
  secondPos2.x = IN.texCoord.x + (sin(PI * IN.texCoord.x) * offset2.x);
  secondPos2.y = IN.texCoord.y + (sin(PI * IN.texCoord.y) * offset2.y);

  gl_FragColor = (texture(diffuseTex, secondPos2) + texture(diffuseTex, secondPos1))/2;
}
