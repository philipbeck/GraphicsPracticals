#version 330 core

uniform sampler2D diffuseTex;
uniform sampler2D bumpTex;
uniform sampler2D diffuseTex2;
uniform sampler2D bumpTex2;
uniform sampler2D diffuseTex3;
uniform sampler2D bumpTex3;

uniform vec3 cameraPos;
uniform vec4 lightColour;
uniform vec3 lightPos;
uniform float lightRadius;

in Vertex{
  vec4 colour;
  vec2 texCoord;
  vec3 normal;
  vec3 tangent;
  vec3 binormal;
  vec3 worldPos;
} IN;

out vec4 gl_FragColor;

//lots of parameters
vec4 blendTexture(vec4 tex1, vec4 tex2, float minHeight, float areaSize){
  //makes a fraction between 0 and 90 degrees
  float fraction = 3.14159 * (IN.worldPos.y - minHeight)/(areaSize * 2);
  //sin square + cos squared = 1 so this makes the sections blend evenly
  return (tex1 * pow(sin(fraction), 2) + tex2 * pow(cos(fraction), 2));
}

void main(void){
  vec4 diffuse;
  //gets made before diffuse so the normals can be made at the same time
  mat3 TBN = mat3(IN.tangent, IN.binormal, IN.normal);
  vec3 normal = IN.normal;
  if(IN.worldPos.y > 350){
    diffuse = texture(diffuseTex, IN.texCoord);
    normal = normalize(TBN * (texture(bumpTex, IN.texCoord).rgb * 2.0 - 1.0));
  }
  else if(IN.worldPos.y > 140){
    diffuse = texture(diffuseTex2, IN.texCoord);
    normal = normalize(TBN * (texture(bumpTex2, IN.texCoord).rgb * 2.0 - 1.0));
  }
  else if(IN.worldPos.y > 90){
    vec4 tex1 = texture(diffuseTex2, IN.texCoord);
    vec4 tex2 = texture(diffuseTex3, IN.texCoord);
    diffuse = blendTexture(tex1, tex2, 90, 50);
    tex1 = texture(bumpTex2, IN.texCoord);
    tex2 = texture(bumpTex3, IN.texCoord);
    normal = normalize(TBN * (blendTexture(tex1, tex2, 90, 50).rgb * 2.0 - 1.0));
  }
  else{
    diffuse = texture(diffuseTex3, IN.texCoord);
    normal = normalize(TBN * (texture(bumpTex3, IN.texCoord).rgb * 2.0 - 1.0));
  }

  //TODO do this twice and blend the two together for the transitions between places
  vec3 incident = normalize(lightPos - IN.worldPos);
  float lambert = max(0.0, dot(incident, normal));

  float dist = length(lightPos - IN.worldPos);
  float atten = 1.0 - clamp(dist/lightRadius,0.0,1.0);

  vec3 viewDir = normalize(cameraPos - IN.worldPos);
  vec3 halfDir = normalize(incident + viewDir);

  float rFactor = max(0.0, dot(halfDir, normal));
  float sFactor = pow(rFactor, 50.0);

  vec3 colour = (diffuse.rgb * lightColour.rgb);
  colour += (lightColour.rgb * sFactor) * 0.33;

  gl_FragColor = vec4(colour*atten*lambert, diffuse.a);
  gl_FragColor.rgb += (diffuse.rgb * lightColour.rgb) * 0.3;
}
