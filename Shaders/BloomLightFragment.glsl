#version 150 core

uniform sampler2D diffuseTex;

in Vertex {
	vec2 texCoord;
	vec4 colour;
} IN;

out vec4 gl_FragColor;

void main(void){
  vec4 pixel = texture(diffuseTex, IN.texCoord);
	int power = 4;
  gl_FragColor = vec4(pow(pixel.r,power), pow(pixel.g,power), pow(pixel.b,power),1);
}
