#version 150
uniform sampler2D diffuseTex;

uniform vec2 pixelSize;

in Vertex {
	vec2 	texCoord;
  vec4 	colour;
} IN;

out vec4 gl_FragColor;

void main(void){
  //2 is how wide the pixels are, change to a none whole number to make the screen
  //look slightly broken
  int pixelNumber = int(IN.texCoord/(pixelSize.x*2));
  //might want to blend some colours together
  if(pixelNumber%3 == 0){
    gl_FragColor = vec4(texture(diffuseTex, IN.texCoord).r,0,0,1);
  }
  else if(pixelNumber%3 == 1){
    gl_FragColor = vec4(0,texture(diffuseTex, IN.texCoord).g,0,1);
  }
  else{
    gl_FragColor = vec4(0,0,texture(diffuseTex, IN.texCoord).b,1);
  }
	gl_FragColor.rgb *= 1.3;
}
