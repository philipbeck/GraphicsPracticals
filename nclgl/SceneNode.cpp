#include "SceneNode.h"

//needs declaring here so the compiler knows it exists
GLuint* SceneNode::finalBuffer;

//constructor
SceneNode::SceneNode(Mesh* mesh, Vector4 colour) {
	this->mesh = mesh;
	this->colour = colour;
	parent = NULL;
	modelScale = Vector3(1, 1, 1);
	boundingRadius = 1.0f;
	distanceFromCamera = 0.0f;
	//assume there isn't a shader
	shader = NULL;
}

//destructor
SceneNode::~SceneNode(void) {
	for (unsigned int i = 0; i < children.size(); i++) {
		delete children[i];
	}
}

void SceneNode::AddChild(SceneNode* s) {
	children.push_back(s);
	s->parent = this;
}


void SceneNode::Draw(const OGLRenderer &r) {
	if (mesh) {
		mesh->Draw();
	}
}

void SceneNode::Update(float msec) {
	if (parent) {//if the node has a parent
		//add all of the transforms from the parent on to this one first
		//eg so a hand doesn't leave without it's fingers
		worldTransform = parent->worldTransform * transform;
	}
	else {
		worldTransform = transform;
	}
	//perform tranformation on all children
	for (vector<SceneNode*>::iterator i = children.begin(); i != children.end(); i++) {
		(*i)->Update(msec);
	}
}


bool SceneNode::CompareByCameraDistance(SceneNode*a, SceneNode*b) {
	return (a->distanceFromCamera < b->distanceFromCamera);
}