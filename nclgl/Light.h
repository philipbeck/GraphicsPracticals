#pragma once
#ifndef LIGHT_H
#define LIGHT_H

#include "Vector3.h"
#include "Vector4.h"

class Light {
public:

	Light(Vector3 position, Vector4 colour, float radius):
	position(position),
	colour(colour),
	radius(radius){}
	//defualt
	Light() : Light(Vector3(0,0,0), Vector4(1,1,1,1), 0.0){};

	~Light(void) {}

	inline Vector3 GetPosition() const { return position; }
	inline void SetPosition(Vector3 pos) { position = pos; }

	inline Vector4 GetColour() const { return colour; }
	inline void SetColour(Vector4 col) { colour = col; }

	inline float GetRadius() const { return radius; }
	inline void SetRadius(float r) { radius = r; }
protected:
	Vector3 position;
	Vector4 colour;
	float radius;
};

#endif