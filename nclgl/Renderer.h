#pragma once
#ifndef RENDERER_H
#define RENDERER_H

#include <algorithm>
#include <time.h>

#include "OGLRenderer.h"
#include "Camera.h"
#include "CubeRobot.h"
#include "MD5Node.h"
#include "HeightMap.h"

#define POST_PASSES 1

class Renderer :public OGLRenderer {
public:
	Renderer(Window &parent);
	~Renderer(void);

	virtual void RenderScene();
	virtual void UpdateScene(float msec);

	void SwitchToPerspective();
	void SwitchToOrthographic();

	//Toggling stuff
	void ToggleObject();
	void ToggleDepth();
	void ToggleAlphaBlend();
	void ToggleBlendMode();
	
	//not sure if this should be kept
	void UpdateTextureMatrix(float rotation);

	inline void SetScale(float s) { scale = s; }
	inline void SetRotation(float r) { rotation = r; }
	inline void SetFov(float f) { fov = f; }
protected:
	//drawing scene node
	void DrawNode(SceneNode*n);
	//tutorial 7
	void DrawNodes();
	void BuildNodeLists(SceneNode* from);
	void SortNodeLists();
	void ClearNodeLists();

	vector<SceneNode*> transparentNodeList;
	vector<SceneNode*> nodeList;

	//quad mesh
	Mesh* quad;
	//scene node
	SceneNode* root;

	//Camera object
	Camera* camera;

	//texture stuff
	bool repeating;
	bool filtering;

	//tutorial 4
	bool modifyObject;
	bool usingDepth;
	bool usingAlpha;
	int blendMode;

	//tutorial 9
	MD5FileData* hellData;
	MD5Node* hellNode;

	//tutorial 10
	void PresentScene();
	void DrawPostProcess();
	void DrawSinglePostProcess();
	void DrawScene();

	Shader* sceneShader;
	Shader* combineShader;

	HeightMap* heightMap;

	GLuint bufferFBO;
	GLuint processFBO;
	GLuint bufferColourTex[2];
	GLuint bufferDepthTex;

	//tutorial 11
	Light* light;

	//tutorial 13
	void DrawHeightMap();
	void DrawWater();
	void DrawSkyBox();

	GLuint cubeMap;

	Mesh* waterQuad;

	Shader* lightShader;
	Shader* reflectShader;
	Shader* skyboxShader;

	float waterRotate;

	//my stuff
	float fov;
	
	float time;
	
	//not sure if this should stay
	float scale;
	float rotation;
};

#endif