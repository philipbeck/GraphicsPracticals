#pragma once
#ifndef PLANE_H
#define PLANE_H

#include "vector3.h"

class Plane {
public:
	//constructors and destructor
	Plane(void) {}
	Plane(const Vector3 &normal, float distance, bool normalise = false);
	~Plane(void) {}
	//normal stuff
	inline void SetNormal(const Vector3 &normal) { this->normal = normal; }
	inline Vector3 GetNormal() const { return normal; }

	inline void SetDistance(float d) { distance = d; }
	inline float GetDistance() const { return distance; }

	bool SphereInPlane(const Vector3 &position, float radius) const;
protected:
	Vector3 normal;
	float distance;
};

#endif