#pragma once
#ifndef SHADER_H

#include "OGLRenderer.h"

//defines instead of enums as there will never be more values added to this
#define SHADER_VERTEX 0
#define SHADER_FRAGMENT 1
#define SHADER_GEOMETRY 2

using namespace std;

class Shader {
public:
	//constructor takes in strings of shader code to be compiled
	Shader(string vertex, string fragment, string geometry = "");
	~Shader(void);

	inline GLuint GetProgram() { return program; }
	bool LinkProgram();
protected:
	void SetDefualtAttributes();
	bool LoadShaderFile(string from, string &into);
	GLuint GenerateShader(string from, GLenum type);

	GLuint objects[3];
	GLuint program;

	bool loadFailed;
};


#endif // !SHADER_H
