#pragma once
#ifndef MESH_H
#define MESH_H

#include "OGLRenderer.h"

enum MeshBuffer {
	VERTEX_BUFFER,
	COLOUR_BUFFER,
	TEXTURE_BUFFER,
	NORMAL_BUFFER,
	TANGENT_BUFFER,
	INDEX_BUFFER,
	MAX_BUFFER//goes right at the end and is how many things are in mesh buffer
};

class Mesh {
public:
	//constructor
	Mesh(void);
	//destructor
	virtual ~Mesh(void);
	//other stuff
	virtual void Draw();
	static Mesh* GenerateTriangle();
	static Mesh* GenerateQuad();
	static Mesh* GenerateLine();
	//texture stuff
	inline void SetTexture(GLuint tex) { texture = tex; }
	inline GLuint GetTexture() { return texture; }
	//bump map stuff
	inline void SetBumpMap(GLuint tex) { bumpTexture = tex; }
	inline GLuint GetBumpMap() { return bumpTexture; }
protected:
	void GenerateNormals();
	void GenerateTangents();
	void BufferData();

	Vector3 GenerateTangent(const Vector3 &a, const Vector3 &b, const Vector3 &c, 
		const Vector2 &ta, const Vector2 &tb, const Vector2 &tc);

	GLuint arrayObject;
	GLuint bufferObject[MAX_BUFFER];
	GLuint numVertices;
	GLuint numIndices;
	GLuint type;

	unsigned int* indices;
	Vector3* vertices;
	Vector3* normals;
	Vector3* tangents;
	Vector4* colours;

	GLuint bumpTexture;

	//texture stuff
	GLuint texture;
	Vector2* textureCoords;
};

#endif