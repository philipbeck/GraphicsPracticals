#pragma once
#ifndef HEIGHT_MAP
#define	HEIGHT_MAP

#include <string>
#include <iostream>
#include <fstream>

#include "Mesh.h"

#define RAW_WIDTH 257
#define RAW_HEIGHT 257

#define HEIGHTMAP_X 16.0f
#define HEIGHTMAP_Z 16.0f
#define HEIGHTMAP_Y 4.0f
#define HEIGHTMAP_TEX_X 1.0f/HEIGHTMAP_X
#define HEIGHTMAP_TEX_Z 1.0f/HEIGHTMAP_Z

class HeightMap :public Mesh {
public:
	HeightMap(std::string name);
	~HeightMap(void) {};

	void Draw();

	Vector3 GetPoint();
	Vector3 GetPoint(int x, int y);
	Vector3 GetNormal(int x, int y);


	inline void SetTexture2(GLuint texture) { texture2 = texture; }
	inline GLuint GetTexture2() { return texture2; }

	inline void SetBumpMap2(GLuint texture) { bumpMap2 = texture; }
	inline GLuint GetBumpMap2() { return bumpMap2; }


	inline void SetTexture3(GLuint texture) { texture3 = texture; }
	inline GLuint GetTexture3() { return texture3; }

	inline void SetBumpMap3(GLuint texture) { bumpMap2 = texture; }
	inline GLuint GetBumpMap3() { return bumpMap2; }
protected:
	GLuint texture2;
	GLuint bumpMap2;
	GLuint texture3;
	GLuint bumpMap3;
};

#endif