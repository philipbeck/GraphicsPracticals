#include "Shader.h"

Shader::Shader(string vFile, string fFile, string gFile) {
	cout << "Compiling Shader..." << endl;

	program = glCreateProgram();
	objects[SHADER_VERTEX] = GenerateShader(vFile, GL_VERTEX_SHADER);
	objects[SHADER_FRAGMENT] = GenerateShader(fFile, GL_FRAGMENT_SHADER);
	objects[SHADER_GEOMETRY] = 0;

	if (!gFile.empty()) {
		objects[SHADER_GEOMETRY] = GenerateShader(gFile, GL_GEOMETRY_SHADER);
		glAttachShader(program, objects[SHADER_GEOMETRY]);
	}

	glAttachShader(program, objects[SHADER_VERTEX]);
	glAttachShader(program, objects[SHADER_FRAGMENT]);

	SetDefualtAttributes();
}

Shader::~Shader(void) {
	for (int i = 0; i < 3; ++i) {
		glDetachShader(program, objects[i]);
		glDeleteShader(objects[i]);
	}
	glDeleteProgram(program);
}

bool	Shader::LoadShaderFile(string from, string &into) {
	ifstream	file;
	string		temp;

	file.open(from.c_str());
	if (!file.is_open()) {
		cout << "\t Error: File does not exist!" << endl;
		return false;
	}

	while (!file.eof()) {
		getline(file, temp);
		into += temp + "\n";
	}

	//cout << into << endl << endl;

	file.close();
	//cout << "Loaded shader text!" << endl;
	return true;
}

GLuint	Shader::GenerateShader(string from, GLenum type) {
	cout << "\t-> Compiling Shader: " << from << endl;

	string load;
	if (!LoadShaderFile(from, load)) {
		loadFailed = true;
		return 0;
	}

	GLuint shader = glCreateShader(type);

	const char *chars = load.c_str();
	glShaderSource(shader, 1, &chars, NULL);
	glCompileShader(shader);

	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

	if (status == GL_FALSE) {
		cout << "\t Error: Compiling failed!" << endl;
		char error[512];
		glGetInfoLogARB(shader, sizeof(error), NULL, error);
		cout << error << endl << endl;
		loadFailed = true;
		cout << endl;
		return 0;
	}

	loadFailed = false;
	return shader;
}

bool Shader::LinkProgram() {
	if (loadFailed) {
		return false;
	}
	glLinkProgram(program);
	cout << "\t-> Linking Shader: ";

	GLint code;
	glGetProgramiv(program, GL_LINK_STATUS, &code);

	if (code == GL_FALSE) {
		cout << "Failed!" << endl;
		int max = 512;
		char error[512];
		glGetProgramInfoLog(program, max, &max, &error[0]);
		cout << error << endl << endl;
		loadFailed = true;
		return false;
	}

	cout << "Success!" << endl << endl;
	return true;
}

void	Shader::SetDefualtAttributes() {
	glBindAttribLocation(program, VERTEX_BUFFER, "position");
	glBindAttribLocation(program, COLOUR_BUFFER, "colour");
	glBindAttribLocation(program, NORMAL_BUFFER, "normal");
	glBindAttribLocation(program, TANGENT_BUFFER, "tangent");
	glBindAttribLocation(program, TEXTURE_BUFFER, "texCoord");

	glBindAttribLocation(program, MAX_BUFFER + 1, "transformIndex");
}

//#include "Shader.h"
//
////constructor
//Shader::Shader(string vFile, string fFile, string gFile) {
//	program = glCreateProgram();
//	//generate shaders
//	objects[SHADER_VERTEX] = GenerateShader(vFile, GL_VERTEX_SHADER);
//	objects[SHADER_FRAGMENT] = GenerateShader(fFile, GL_FRAGMENT_SHADER);
//	objects[SHADER_GEOMETRY] = 0;//don't bother with a geomentry shader until we know it exists
//	//if the geomentry shader exists
//	if (!gFile.empty()) {
//		objects[SHADER_GEOMETRY] = GenerateShader(fFile, GL_GEOMETRY_SHADER);
//		glAttachShader(program, objects[SHADER_GEOMETRY]);
//	}
//	//always add the other values
//	glAttachShader(program, objects[SHADER_VERTEX]);
//	glAttachShader(program, objects[SHADER_FRAGMENT]);
//
//	SetDefualtAttributes();
//}
//
//Shader::~Shader(void) {
//	for (int i = 0; i < 3; i++) {
//		glDetachShader(program, objects[i]);
//		glDeleteShader(objects[i]);
//	}
//	glDeleteProgram(program);
//}
//
//
//bool Shader::LinkProgram() {
//	if (loadFailed) {
//		//if loading a program didn't happen don't try to link it
//		return false;
//	}
//	glLinkProgram(program);
//
//	GLint code;
//	glGetProgramiv(program, GL_LINK_STATUS, &code);
//	if (code == GL_FALSE) {
//		cout << "LINK FAILED!!!!\n\n\n";
//
//		GLint maxLength = 0;
//		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);
//
//		//The maxLength includes the NULL character
//		std::vector<GLchar> infoLog(maxLength);
//		glGetProgramInfoLog(program, maxLength, &maxLength, &infoLog[0]);
//
//		for (int i = 0; i < infoLog.size(); i++) {
//			cout << infoLog[i];
//		}
//	}
//	return code == GL_TRUE ? true : false;
//}
//
////private stuff*********************
////set defualt attributes
//void Shader::SetDefualtAttributes() {
//	//give the values the shader needs to look at variable names
//	glBindAttribLocation(program, VERTEX_BUFFER, "position");
//	glBindAttribLocation(program, COLOUR_BUFFER, "colour");
//	glBindAttribLocation(program, NORMAL_BUFFER, "normal");
//	glBindAttribLocation(program, TANGENT_BUFFER, "tangent");
//	glBindAttribLocation(program, TEXTURE_BUFFER, "texCoord");
//}
//
//bool Shader::LoadShaderFile(string from, string &into) {
//	ifstream file;
//	string temp;
//
//	cout << "Loading shadder test from " << from << "\n\n";
//
//	file.open(from.c_str());
//	if (!file.is_open()) {
//		cout << "File does not exist!\n";
//		return false;
//	}
//
//	while (!file.eof()) {
//		getline(file, temp);
//		into += temp + "\n";
//	}
//
//	file.close();
//
//	cout << into << "\n\n";
//	cout << "loaded shader to text!\n\n";
//
//	return true;
//
//}
//
//GLuint Shader::GenerateShader(string from, GLenum type) {
//	cout << "Compiling Shader...\n";
//
//	string load;
//	if (!LoadShaderFile(from, load)) {
//		cout << "Compiling Failed!\n";
//		loadFailed = true;
//		return 0;
//	}
//	//make the shader
//	GLuint shader = glCreateShader(type);
//
//	const char *chars = load.c_str();
//	glShaderSource(shader, 1, &chars, NULL);
//	glCompileShader(shader);
//
//	GLint status;
//	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
//
//	if (status == GL_FALSE) {
//		cout << "Compiling Failed!\n";
//		char error[512];
//		glGetInfoLogARB(shader, sizeof(error), NULL, error);
//		cout << error;
//		loadFailed = true;
//		return 0;
//	}
//	cout << "Compiling success!\n\n";
//	loadFailed = false;
//	return shader;
//}