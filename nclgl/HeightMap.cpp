#include "HeightMap.h"

HeightMap::HeightMap(std::string name) {
	std::ifstream file(name.c_str(), ios::binary);
	//check if file exists
	if (!file) {
		return;
	}
	numVertices = RAW_WIDTH * RAW_HEIGHT;
	numIndices = (RAW_WIDTH - 1)*(RAW_HEIGHT - 1) * 6;
	vertices = new Vector3[numVertices];
	textureCoords = new Vector2[numVertices];
	indices = new GLuint[numIndices];

	unsigned char*data = new unsigned char[numVertices];
	file.read((char*)data, numVertices * sizeof(unsigned char));
	file.close();

	for (int x = 0; x < RAW_WIDTH; x++) {
		for (int z = 0; z < RAW_HEIGHT; z++) {
			int offset = (x * RAW_WIDTH) + z;
			//vertices
			vertices[offset] = Vector3(
				x*HEIGHTMAP_X,
				data[offset] * HEIGHTMAP_Y,
				z * HEIGHTMAP_Z);
			//texture coords
			textureCoords[offset] = Vector2(
				x * HEIGHTMAP_TEX_X,
				z * HEIGHTMAP_TEX_Z);
		}
	}

	//delete data to avoid memory leaks
	delete data;

	numIndices = 0;

	for (int x = 0; x < RAW_WIDTH - 1; x++) {
		for (int z = 0; z < RAW_HEIGHT - 1; z++) {
			int a = (x * RAW_WIDTH) + z;
			int b = ((x + 1) * RAW_WIDTH) + z;
			int c = ((x + 1) * RAW_WIDTH) + (z + 1);
			int d = (x * RAW_WIDTH) + (z + 1);

			indices[numIndices++] = c;
			indices[numIndices++] = b;
			indices[numIndices++] = a;

			indices[numIndices++] = a;
			indices[numIndices++] = d;
			indices[numIndices++] = c;
		}
	}

	GenerateNormals();
	GenerateTangents();

	texture2 = NULL;
	bumpMap2 = NULL;
	texture3 = NULL;
	bumpMap3 = NULL;

	BufferData();
}


Vector3 HeightMap::GetPoint() {
	return vertices[(long)(rand()*rand()) % (numVertices)];
}

Vector3 HeightMap::GetPoint(int x, int y) {
	return vertices[(x * RAW_WIDTH) + y];
}

Vector3 HeightMap::GetNormal(int x, int y) {
	return normals[(x * RAW_WIDTH) + y];
}

void HeightMap::Draw() {
	Mesh::Draw();
	if (texture2) {
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, texture2);
	}
	if (bumpMap2) {
		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, bumpMap2);
	}	
	if (texture3) {
		glActiveTexture(GL_TEXTURE4);
		glBindTexture(GL_TEXTURE_2D, texture3);
	}
	if (bumpMap3) {
		glActiveTexture(GL_TEXTURE5);
		glBindTexture(GL_TEXTURE_2D, bumpMap3);
	}
}