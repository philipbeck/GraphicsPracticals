#pragma once
#ifndef CUBEROBOT_H
#define CUBEROBOT

#include "SceneNode.h"
#include "OBJMesh.h"

class CubeRobot : public SceneNode {
public:
	CubeRobot(void);
	~CubeRobot(void) {};
	//override update
	virtual void Update(float msec);

	static void CreateCube();

	inline static void DeleteCube() { delete cube; }

protected:
	//every cube robot has this mesh
	static Mesh* cube;
	SceneNode* head;
	SceneNode* leftArm;
	SceneNode* rightArm;
};

#endif