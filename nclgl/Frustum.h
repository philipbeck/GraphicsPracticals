#pragma once
#ifndef FRUSTUM_H
#define FRUSTUM_H

#include "Plane.h"
#include "Matrix4.h"
#include "SceneNode.h"
//compiles the matrix4 class first?
class Matrix4;


class Frustum {
public:
	//constructor and destructor
	Frustum(void) {}
	~Frustum(void) {}
	
	void FromMatrix(const Matrix4 &mvp);
	bool InsideFrustum(SceneNode&n);
protected:
	Plane planes[6];
};

#endif