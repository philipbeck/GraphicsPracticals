#pragma once
#ifndef SCENENODE_H
#define SCENENODE_H

#include "Matrix4.h"
#include "Vector3.h"
#include "Vector4.h"
#include "Mesh.h"

#include <vector>

class SceneNode {
public:
	//constructor and destructor
	SceneNode(Mesh*m = NULL, Vector4 colour = Vector4(1, 1, 1, 1));
	~SceneNode(void);

	//bounding business
	inline float GetBoundingRadius() const { return boundingRadius; }
	inline void SetBoundingRadius(float r) { boundingRadius = r; }

	//distance from camera
	inline float GetCameraDistance() const { return distanceFromCamera; }
	inline void SetCameraDistance(float d) { distanceFromCamera = d; }

	//transform stuff
	inline void SetTransform(const Matrix4 &matrix) { transform = matrix; }
	inline const Matrix4& GetTransform() const { return transform; }
	inline Matrix4 GetWorldTransform() const { return worldTransform; }

	//colour stuff
	inline Vector4 GetColour() const { return colour; }
	inline void SetColour(Vector4 c) { colour = c; }

	//scal stuff
	inline Vector3 GetModelScale() const { return modelScale; }
	inline void SetModelScale(Vector3 s) { modelScale = s; }

	//mesh stuff
	inline Mesh* GetMesh() const { return mesh; }
	inline void SetMesh(Mesh*m) { mesh = m; }

	//children iterators
	inline std::vector<SceneNode*>::const_iterator GetChildIteratorStart() { return children.begin(); }
	inline std::vector<SceneNode*>::const_iterator GetChildIteratorEnd() { return children.end(); }

	//adding a child
	void AddChild(SceneNode* s);

	virtual void Update(float msec);
	virtual void Draw(const OGLRenderer &r);

	//compare by camera distance
	static bool CompareByCameraDistance(SceneNode*a, SceneNode*b);

	inline GLuint GetShader() const { return shader; }
	inline void SetShader(GLuint s) { shader = s; }

	//set the final buffer
	inline static void SetFinalBuffer(GLuint& b) { finalBuffer = &b; }
	//so the world node can draw it's children with it's own shader
	friend class WorldNode;
protected:
	SceneNode* parent;
	//referece to a mesh so only one of each mesh needs to exist
	Mesh* mesh;
	Matrix4 worldTransform;
	Matrix4 transform;
	Vector3 modelScale;
	Vector4 colour;
	std::vector<SceneNode*> children;
	float distanceFromCamera;
	float boundingRadius;
	//shader stuff
	GLuint shader;
	//final buffer to draw in
	static GLuint* finalBuffer;
};

#endif