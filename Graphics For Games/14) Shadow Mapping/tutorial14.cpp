#pragma comment(lib, "nclgl.lib")

#include "../NCLGL/window.h"
#include "Renderer.h"

int main() {
	Window w("Shadow Mapping! MD5 mesh courtesy of http://www.katsbits.com/", 800,600,false); //This is all boring win32 window creation stuff!
	if(!w.HasInitialised()) {
		return -1;
	}
	
	Renderer renderer(w); //This handles all the boring OGL 3.2 initialisation stuff, and sets up our tutorial!
	if(!renderer.HasInitialised()) {
		return -1;
	}

	w.LockMouseToWindow(true);
	w.ShowOSPointer(false);

	while(w.UpdateWindow() && !Window::GetKeyboard()->KeyDown(KEYBOARD_ESCAPE)){
		if (Window::GetKeyboard()->KeyDown(KEYBOARD_U)) {
			renderer.MoveLight(Vector3(0, 0, 1));
		}
		if (Window::GetKeyboard()->KeyDown(KEYBOARD_J)) {
			renderer.MoveLight(Vector3(0, 0, -1));
		}
		if (Window::GetKeyboard()->KeyDown(KEYBOARD_H)) {
			renderer.MoveLight(Vector3(1, 0, 0));
		}
		if (Window::GetKeyboard()->KeyDown(KEYBOARD_K)) {
			renderer.MoveLight(Vector3(-1, 0, 0));
		}
		if (Window::GetKeyboard()->KeyDown(KEYBOARD_Y)) {
			renderer.MoveLight(Vector3(0, 1, 0));
		}
		if (Window::GetKeyboard()->KeyDown(KEYBOARD_I)) {
			renderer.MoveLight(Vector3(0, -1, 0));
		}

		renderer.UpdateScene(w.GetTimer()->GetTimedMS());
		renderer.RenderScene();
	}

	return 0;
}