#pragma once
#ifndef WORLD_H
#define WORLD_H

#include "../../nclgl/SceneNode.h"

//try 512 to start with
#define SHADOW_SIZE 4096

//this is the ground and the sun
class WorldNode : public SceneNode {
public:
	WorldNode(GLsizei& width, GLsizei& height, Mesh*m = NULL, Vector4 colour = Vector4(1, 1, 1, 1));
	virtual ~WorldNode(void);

	void Draw(const OGLRenderer &r);

	inline void SetShadowShader(GLuint s) { shadowShader = s; };

	inline void MoveSun(Vector3 pos) { sun->SetPosition(pos); }
	inline void SetSunRadius(float r) { sun->SetRadius(r); }
private:
	//void Draw Shadows
	void DrawShadowScene();
	void DrawCombinedScene();
	//for now the only thing in the sceme that is going to cast shadows
	Light* sun;
	GLuint shadowTex;
	GLuint shadowFBO;
	//shadow shader
	GLuint shadowShader;
	//pointer to projection matrix
	Matrix4* projMatrix;
	GLsizei* width;
	GLsizei* height;
};

#endif