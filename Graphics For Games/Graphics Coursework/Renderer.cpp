#include "Renderer.h"

#include "../../nclgl/OBJMesh.h"

Renderer::Renderer(Window &parent) :OGLRenderer(parent) {
	//global stuff!
	//make camera
	camera = new Camera(0.0f, 225.0f, Vector3(250.0f, 800.0f, 250.0f));
	camera->SetSpeed(1.0);
	//quad for post processing
	quad = Mesh::GenerateQuad();
	//load knight
	knightData = new MD5FileData(MESHDIR"hellknight.md5mesh");

	//Making all of the shaders!
	currentShader = new Shader(SHADERDIR"TexturedVertex.glsl", SHADERDIR"TexturedFragment.glsl");
	if (!currentShader->LinkProgram()) {
		return;
	}
	lightningShader = new Shader(SHADERDIR"LighteningVertex.glsl", SHADERDIR"LighteningFrag.glsl", SHADERDIR"LighteningGeometry.glsl");
	if (!lightningShader->LinkProgram()) {
		return;
	}
	bumpShader = new Shader(SHADERDIR"BumpVertex.glsl", SHADERDIR"BumpIslandFragment.glsl");
	if (!bumpShader->LinkProgram()) {
		return;
	}
	//works for multiple lights
	bumpShader2 = new Shader(SHADERDIR"BumpVertex.glsl", SHADERDIR"BumpFragmentMultiple.glsl");
	if (!bumpShader2->LinkProgram()) {
		return;
	}
	//hellknight shader
	knightShader = new Shader(SHADERDIR"SkeletonVertex.glsl", SHADERDIR"SkeletonFragment.glsl");
	if (!knightShader->LinkProgram()) {
		return;
	}
	//skybox shader
	skyboxShader = new Shader(SHADERDIR"skyboxVertex.glsl", SHADERDIR"skyboxFragment.glsl");
	if (!skyboxShader->LinkProgram()) {
		return;
	}
	waterShader = new Shader(SHADERDIR"BumpVertex.glsl", SHADERDIR"bumpReflectFragment.glsl");
	if (!waterShader->LinkProgram()) {
		return;
	}
	//world test**********************************************************
	sceneShader = new Shader(SHADERDIR"shadowSceneVert.glsl", SHADERDIR"shadowSceneFrag.glsl");
	if (!sceneShader->LinkProgram()) {
		return;
	}
	shadowShader = new Shader(SHADERDIR"shadowVert.glsl", SHADERDIR"shadowFrag.glsl");
	if (!shadowShader->LinkProgram()) {
		return;
	}

	particleShader = new Shader(SHADERDIR"ParticleVertex.glsl", SHADERDIR"ParticleFragment.glsl", SHADERDIR"ParticleGeometry.glsl");
	if (!particleShader->LinkProgram()) {
		return;
	}
	//scene 1 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
	//scene 1 light
	light = new Light(Vector3(2000.0, 500.0, 2000.0), Vector4(1.0, 1.0, 0.9, 1.0), 5000.0);
	//make root scene node
	Lightning::MakeLightning();
	//quad for water in scene 1
	waterQuad = Mesh::GenerateQuad();
	waterQuad->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"Water.jpg",
		SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	waterQuad->SetBumpMap(SOIL_load_OGL_texture(TEXTUREDIR"WaterBump.png",
		SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	SetTextureRepeating(waterQuad->GetTexture(), true);
	SetTextureRepeating(waterQuad->GetBumpMap(), true);
	//scene 1 skybox
	skyBox[0] = SOIL_load_OGL_cubemap(
		TEXTUREDIR"rusted_west.jpg", TEXTUREDIR"rusted_east.jpg",
		TEXTUREDIR"rusted_up.jpg", TEXTUREDIR"rusted_down.jpg",
		TEXTUREDIR"rusted_south.jpg", TEXTUREDIR"rusted_north.jpg",
		SOIL_LOAD_RGB, SOIL_CREATE_NEW_ID, 0);

	//heightmap of the galapagos islands, uses a lot of textures
	heightMap1 = new HeightMap(TEXTUREDIR"GalapagosHeightMap.raw");
	heightMap1->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"Barren Reds.jpg",
		SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	heightMap1->SetBumpMap(SOIL_load_OGL_texture(TEXTUREDIR"Barren RedsDOT3.jpg",
		SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	heightMap1->SetTexture2(SOIL_load_OGL_texture(TEXTUREDIR"Grass.jpg",
		SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	heightMap1->SetBumpMap2(SOIL_load_OGL_texture(TEXTUREDIR"GrassBump.png",
		SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	heightMap1->SetTexture3(SOIL_load_OGL_texture(TEXTUREDIR"Sand.jpg",
		SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	heightMap1->SetBumpMap3(SOIL_load_OGL_texture(TEXTUREDIR"SandBump.png",
		SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	if (!heightMap1->GetTexture()) {
		return;
	}
	SetTextureRepeating(heightMap1->GetTexture(), true);
	SetTextureRepeating(heightMap1->GetBumpMap(), true);
	SetTextureRepeating(heightMap1->GetTexture2(), true);
	SetTextureRepeating(heightMap1->GetBumpMap2(), true);
	SetTextureRepeating(heightMap1->GetTexture3(), true);
	SetTextureRepeating(heightMap1->GetBumpMap3(), true);

	scenes[0] = new SceneNode();
	SceneNode* Scene1Ground = new SceneNode();
	Scene1Ground->SetColour(Vector4(1,0,0,1));
	Scene1Ground->SetMesh(heightMap1);
	Scene1Ground->SetBoundingRadius(4000);
	scenes[0] = Scene1Ground;
	for (int i = 0; i < LIGHTINGBOLTS; i++) {
		lightning[i] = new Lightning();
		lightning[i]->SetModelScale(Vector3(600.0, 1000.0, 600.0));
		lightning[i]->SetShader(lightningShader->GetProgram());
		lightning[i]->SetBoundingRadius(1000.0);
		scenes[0]->AddChild(lightning[i]);
	}
	//adding water to scene
	water = new Water(waterQuad, &skyBox[0]);
	water->SetShader(waterShader->GetProgram());
	//water scale
	float heightX = (RAW_WIDTH * HEIGHTMAP_X / 2.0f);
	float heightY = 256 * HEIGHTMAP_Y / 3.0f;
	float heightZ = (RAW_HEIGHT * HEIGHTMAP_Z / 2.0f);
	water->SetTransform(Matrix4::Translation(Vector3(heightX, heightY - 295, heightZ)) *
		Matrix4::Rotation(90, Vector3(1.0f, 0.0f, 0.0f)));
	water->SetModelScale(Vector3(heightX, heightZ, 1));
	water->SetBoundingRadius(2056);
	scenes[0]->AddChild(water);

	//making the hellknight for scene 1
	knight = new MD5Node(*knightData);
	//puts the knight somewhere in the world stood on the surface
	knight->SetTransform(Matrix4::Translation(heightMap1->GetPoint()));
	knight->SetModelScale(Vector3(3, 3, 3));
	knight->SetBoundingRadius(500);
	scenes[0]->AddChild(knight);

	Scene1Ground->SetShader(bumpShader->GetProgram());
	knight->SetShader(knightShader->GetProgram());

	knightData->AddAnim(MESHDIR"idle2.md5anim");
	knight->PlayAnim(MESHDIR"idle2.md5anim");
	//scene 2 222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222
	//scene 2 skybox
	skyBox[1] = SOIL_load_OGL_cubemap(
		TEXTUREDIR"SpaceRight.png", TEXTUREDIR"SpaceLeft.png",
		TEXTUREDIR"SpaceTop.png", TEXTUREDIR"SpaceBottom.png",
		TEXTUREDIR"SpaceFront.png", TEXTUREDIR"SpaceBack.png", 
		SOIL_LOAD_RGB, SOIL_CREATE_NEW_ID, 0);
	//scene 2 heightmap
	heightMap2 = new HeightMap(TEXTUREDIR"moonmap.raw");
	heightMap2->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"MoonTexture.jpg",
		SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	heightMap2->SetBumpMap(SOIL_load_OGL_texture(TEXTUREDIR"MoonTexture.jpg",
		SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	SetTextureRepeating(heightMap2->GetTexture(), true);
	SetTextureRepeating(heightMap2->GetBumpMap(), true);

	scenes[1] = new SceneNode();

	worldNode = new WorldNode(width,height);
	worldNode->SetShader(sceneShader->GetProgram());
	worldNode->SetShadowShader(shadowShader->GetProgram());
	worldNode->SetMesh(heightMap2);
	worldNode->SetBoundingRadius(4500);

	//moon robot
	robot = new OBJMesh(MESHDIR"Andy.obj");//they called him andy because he's an android <3
	robot->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"Andy_Diffuse.png",
		SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	for (int i = 0; i < MOONBOT_NUMBER; i++) {
		moonBots[i] = new MoonBot(robot, heightMap2, rand()%RAW_WIDTH, rand()%RAW_HEIGHT);
		moonBots[i]->SetModelScale(Vector3(700, 700, 700));
		moonBots[i]->SetBoundingRadius(500);
		worldNode->AddChild(moonBots[i]);
	}


	scenes[1]->AddChild(worldNode);

	//scene 3 3333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333
	//scene 3 skybox, same as scene 1 for now
	skyBox[2] = SOIL_load_OGL_cubemap(
		TEXTUREDIR"HellMap/px.png", TEXTUREDIR"HellMap/nx.png",
		TEXTUREDIR"HellMap/py.png", TEXTUREDIR"HellMap/ny.png",
		TEXTUREDIR"HellMap/pz.png", TEXTUREDIR"HellMap/nz.png",
		SOIL_LOAD_RGB, SOIL_CREATE_NEW_ID, 0);
	heightMap3 = new HeightMap(TEXTUREDIR"HellHeight.raw");
	heightMap3->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"Rocky.jpg",
		SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	heightMap3->SetBumpMap(SOIL_load_OGL_texture(TEXTUREDIR"Barren RedsDOT3.jpg",
		SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	SetTextureRepeating(heightMap3->GetTexture(), true);
	SetTextureRepeating(heightMap3->GetBumpMap(), true);
	//lava
	lavaQuad = Mesh::GenerateQuad();
	lavaQuad->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"lava.png",
		SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	SetTextureRepeating(lavaQuad->GetTexture(), true);

	//make the root scene node first
	scenes[2] = new SceneNode();
	scenes[2]->SetMesh(heightMap3);
	scenes[2]->SetBoundingRadius(4500);
	scenes[2]->SetShader(bumpShader2->GetProgram());

	//partical emitter
	emitter = new ParticleEmitter();	
	emitter->SetParticleSize(10);
	//add a few particle emitters about the scene
	ParticleNode* particles = new ParticleNode(emitter, particleShader->GetProgram());
	particles->SetTransform(Matrix4::Translation(Vector3(1800, 0, 2000)));
	particles->SetBoundingRadius(2500);
	scenes[2]->AddChild(particles);
	particles = new ParticleNode(emitter, particleShader->GetProgram());
	particles->SetTransform(Matrix4::Translation(Vector3(2900, 0, 900)));
	particles->SetBoundingRadius(3000);
	scenes[2]->AddChild(particles);
	particles = new ParticleNode(emitter, particleShader->GetProgram());
	particles->SetTransform(Matrix4::Translation(Vector3(200, 0, 600)));
	particles->SetBoundingRadius(900);
	scenes[2]->AddChild(particles);

	//make all kinds of hell knights!
	for (int i = 0; i < KNIGHT_NUMBER; i++) {
		knights3[i] = new MD5Node(*knightData);
		knights3[i]->PlayAnim(MESHDIR"idle2.md5anim");
		//update by a random amount so they are all out of synch
		knights3[i]->Update(rand() % 10000);//10000 is over the top but it doesn't do any harm
		knights3[i]->SetBoundingRadius(100);
		Vector3 pos;
		//keep getting points on the map until there is one that is out of the lava
		while (pos.y < 100) {
			pos = heightMap3->GetPoint();
		}
		knights3[i]->SetTransform(Matrix4::Translation(pos) *
			Matrix4::Rotation(rand() % 360, Vector3(0, 1, 0)));
		knights3[i]->SetShader(knightShader->GetProgram());
		scenes[2]->AddChild(knights3[i]);
	}
	//make the lava
	lava = new Water(lavaQuad);
	lava->SetTransform(Matrix4::Translation(Vector3(heightX, heightY - 295, heightZ)) *
		Matrix4::Rotation(90, Vector3(1.0f, 0.0f, 0.0f)));
	lava->SetModelScale(Vector3(heightX, heightZ, 1));
	lava->SetBoundingRadius(2056);
	//make the lava a bit smaller than the texture
	lava->SetTextureMatrix(Matrix4::Scale(Vector3(7,7,7)));
	//give lava the standard shader so it can update it's matrices properly
	lava->SetShader(currentShader->GetProgram());

	lights3 = new Light[5];
	//Filling lights
	lights3[0] = Light(Vector3(2000, 1200, 2000), Vector4(1, 0.2, 0, 1), 5000.0f);
	lights3[1] = Light(Vector3(500, 1200, 500), Vector4(0, 0, 1, 1), 5000.0f);
	lights3[2] = Light(Vector3(2000, 1200, 1000), Vector4(1, 1, 0, 1), 5000.0f);
	lights3[3] = Light(Vector3(3000, 1200, 3000), Vector4(1, 0.7, 0, 1), 5000.0f);
	lights3[4] = Light(Vector3(500, 1200, 2000), Vector4(1, 0.5, 0, 1), 5000.0f);

	scenes[2]->AddChild(lava);

	//POST PROCESSOR STUFF  PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
	//making the first postprocessor
	//this one loads it's own shader
	doublePost = new DoubleVisionPost(quad);

	Shader* crtShader = new Shader(SHADERDIR"TexturedVertex.glsl", SHADERDIR"CRTFragment.glsl");
	if (!crtShader->LinkProgram()) {
		return;
	}
	crtPost = new PerPixelPost(crtShader, quad, 1.0 / width, 1.0 / height);

	//bloom light shader
	Shader* bloomLight = new Shader(SHADERDIR"TexturedVertex.glsl", SHADERDIR"BloomLightFragment.glsl");
	if (!bloomLight->LinkProgram()) {
		return;
	}
	Shader* bloomBlur = new Shader(SHADERDIR"TexturedVertex.glsl", SHADERDIR"BlurFragment.glsl");
	if (!bloomBlur->LinkProgram()) {
		return;
	}
	Shader* bloomCombine = new Shader(SHADERDIR"TexturedVertex.glsl", SHADERDIR"BloomCombineFrag.glsl");
	if (!bloomCombine->LinkProgram()) {
		return;
	}
	bloomPost = new BloomPost(bloomCombine, bloomBlur, bloomLight, quad, width, height);
	//make the info display here since it is effectively a post processor
	font = new Font(SOIL_load_OGL_texture(TEXTUREDIR"tahoma.tga", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_COMPRESS_TO_DXT), 16, 16);
	infoDisplay = new TextDisplay(quad,font);
	
	postProcessors = doublePost;
	postProcessors->PushEnd(bloomPost);
	postProcessors->PushEnd(crtPost);
	postProcessors->PushEnd(infoDisplay);
	//turn double off to start with
	doublePost->ToggleOn();

	//making the buffers
	for (int i = 0; i < 2; i++) {
		glGenTextures(1, &colourBuffers[i]);
		glBindTexture(GL_TEXTURE_2D, colourBuffers[i]);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0,
			GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	}
	//depth texture
	glGenTextures(1, &bufferDepthTex);
	glBindTexture(GL_TEXTURE_2D, bufferDepthTex);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, width, height,
		0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL);
	
	glGenFramebuffers(1, &processFBO);
	glGenFramebuffers(1, &bufferFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, bufferFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
		GL_TEXTURE_2D, bufferDepthTex, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
	 GL_TEXTURE_2D, colourBuffers[0], 0);
	//number variables
	time = 0.0;
	completion = 0.0;
	fov = 45.0;
	freeze = false;
	speed = 1.0;
	//start at scene 1
	currentScene = 0;
	//auto change stuff
	autoCycle = false;
	changeRate = 10000;//every ten seconds
	sceneTime = 0;
	nextKnight = 0;
	//settings
	SwitchToPerspective();
	//enable depth
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);//cullface back to save time
	//skybox business
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

	finalBuffer = postProcessors ? bufferFBO : 0;
	SceneNode::SetFinalBuffer(finalBuffer);

	init = true;
}

Renderer::~Renderer(void) {
	delete camera;
	delete scenes[0];
	delete scenes[1];
	delete scenes[2];
	//TEMP
	delete heightMap1;
	delete heightMap2;
	delete heightMap3;
	delete quad;
	delete waterQuad;
	delete lavaQuad;
	delete robot;

	delete postProcessors;
	//delete shader
	delete lightningShader;
	delete bumpShader;
	delete bumpShader2;
	delete knightShader;
	delete skyboxShader;
	delete waterShader;
	delete particleShader;

	delete light;
	delete[] lights3;

	delete font;
	//hell knight deletion
	delete knightData;

	Lightning::DeleteLightning();

	glDeleteTextures(2, colourBuffers);
	glDeleteFramebuffers(1, &bufferFBO);
	glDeleteFramebuffers(1, &processFBO);
}

void Renderer::UpdateScene(float ms) {
	//update camera
	camera->UpdateCamera(ms);
	//set view matrix
	viewMatrix = camera->BuildViewMatrix();
	//framerate shouldn't be affected by runspeed
	infoDisplay->Update("Framerate: " + std::to_string(1000.0 / (ms)));
	//update frustum
	frameFrustum.FromMatrix(projMatrix * viewMatrix);
	if (!freeze) {
		//don't change the scene if it is frozen
		if (autoCycle) {
			sceneTime += ms;
			if (sceneTime > changeRate) {
				sceneTime = 0;
				NextScene();
			}
		}
		//multiply ms by speed after the camera so moving isn't rediculous at different run speeds
		ms *= speed;
		//increase game time
		time += ms;
		//update scene node
		scenes[currentScene]->Update(ms);
		//update the post processes that need it
		doublePost->Update(ms);
		if (currentScene == 0) {	
			if (rand() % 10 == 0) {
				Strike(rand() % LIGHTINGBOLTS);
			}
			//update knight
			knight->Update(ms);
			water->Update(ms);
		}
		else if (currentScene == 1) {
			for (int i = 0; i < MOONBOT_NUMBER; i++) {
				moonBots[i]->Update(ms);
			}

		}
		else if (currentScene == 2) {
			lava->Update(ms);
			emitter->Update(ms);
			for (int i = 0; i < KNIGHT_NUMBER; i++) {
				knights3[i]->Update(ms);
			}
			if (nextKnight < KNIGHT_NUMBER ) {
				//every 30 frames send a knight flying
				if (rand() % 30 == 0) {
					//big line to send the knight flying in a random direction
					knights3[nextKnight]->SetFlyDir(Vector3((rand() % 100 / 50.0) - 1.0, rand() % 100 / 100.0, (rand() % 100 / 50.0) - 1.0));
					nextKnight++;
				}
			}
			else {
				ResetKnights();
			}
		}
	}
}

void Renderer::RenderScene() {
	//draw straight to screen if there isn't a post processor
	glBindFramebuffer(GL_FRAMEBUFFER, postProcessors ? bufferFBO : 0);
	//clear buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//draw the skybox
	DrawSkybox();

	SwitchToPerspective();

	DrawNode(scenes[currentScene]);

	if (postProcessors) {
		postProcessors->Draw(processFBO, colourBuffers[0], colourBuffers[1]);
	}

	glUseProgram(0);
	SwapBuffers();
}

//protected functions!
void Renderer::UpdateShaderMatrices(GLuint program) {
	glUniformMatrix4fv(glGetUniformLocation(program, "modelMatrix"), 1, false, (float*)&modelMatrix);
	glUniformMatrix4fv(glGetUniformLocation(program, "viewMatrix"), 1, false, (float*)&viewMatrix);
	glUniformMatrix4fv(glGetUniformLocation(program, "projMatrix"), 1, false, (float*)&projMatrix);
	glUniformMatrix4fv(glGetUniformLocation(program, "textureMatrix"), 1, false, (float*)&textureMatrix);
}

void Renderer::SwitchToPerspective() {
	projMatrix = Matrix4::Perspective(0.1f, 10000.0f, (float)width / (float)height, fov);
}


void Renderer::DrawNode(SceneNode* n) {
	if (frameFrustum.InsideFrustum(*n) && n->GetMesh()) {
		glBindFramebuffer(GL_FRAMEBUFFER, finalBuffer);
		//shader program to be used
		GLuint program;
		//if there isn't a shader just use current
		if (n->GetShader()) {
			program = n->GetShader();
		}
		else {
			program = currentShader->GetProgram();
		}
		//update shader anyway
		glUseProgram(program);

		UpdateShaderMatrices(program);

		//all this is just putting uniforms in, in the future the nodes should do this themselves
		Matrix4 transform = n->GetWorldTransform()*Matrix4::Scale(n->GetModelScale());
		if (currentScene == 2) {
			Vector3 p[5];
			Vector4 c[5];
			float r[5];
			int on[5];
			for (int i = 0; i < 5; i++) {
				p[i] = lights3[i].GetPosition();
				c[i] = lights3[i].GetColour();
				r[i] = lights3[i].GetRadius();
				on[i] = true;
			}
			glUniform3fv(glGetUniformLocation(program,
				"lightPos"), 5, (float*)p);
			glUniform4fv(glGetUniformLocation(program,
				"lightColour"), 5, (float*)c);
			glUniform1fv(glGetUniformLocation(program,
				"lightRadius"),5, r);
			glUniform1iv(glGetUniformLocation(program,
				"lightRadius"), 5, on);
		}
		else {
			SetShaderLight(program, *light);
		}
		glUniformMatrix4fv(glGetUniformLocation(program, "modelMatrix"), 1, false, (float*)&transform);
		glUniform4fv(glGetUniformLocation(program, "nodeColour"), 1, (float*)&n->GetColour());
		glUniform1i(glGetUniformLocation(program, "useTexture"), n->GetMesh()->GetTexture());
		glUniform1i(glGetUniformLocation(program, "diffuseTex"), 0);
		glUniform1i(glGetUniformLocation(program, "bumpTex"), 1);
		glUniform1i(glGetUniformLocation(program, "diffuseTex2"), 2);
		glUniform1i(glGetUniformLocation(program, "bumpTex2"), 3);
		glUniform1i(glGetUniformLocation(program, "diffuseTex3"), 4);
		glUniform1i(glGetUniformLocation(program, "bumpTex3"), 5);
		glUniform3fv(glGetUniformLocation(program, "cameraPos"), 1, (float*)&camera->GetPosition());

		//finally draw the node
		n->Draw(*this);
	}
	//iterators are slightly faster that accessing vectors like an array :(
	for (vector<SceneNode*>::const_iterator i = n->GetChildIteratorStart();
		i != n->GetChildIteratorEnd(); i++) {
		DrawNode(*i);
	}
}

void Renderer::Strike(int n) {
	if (!lightning[n]->GetStriking()) {
		lightning[n]->SetTransform(Matrix4::Translation(heightMap1->GetPoint())*
			Matrix4::Rotation(25.0, Vector3((float)(rand() % 100 / 100.0), 0, (float)(rand() % 100 / 100.0))));
		lightning[n]->Strike();
	}
}

void Renderer::SetShaderLight(GLuint shader, const Light &l) {
	glUniform3fv(glGetUniformLocation(shader,
		"lightPos"), 1, (float*)&l.GetPosition());
	glUniform4fv(glGetUniformLocation(shader,
		"lightColour"), 1, (float*)&l.GetColour());
	glUniform1f(glGetUniformLocation(shader,
		"lightRadius"), l.GetRadius());
}

//for moving between scenes
void Renderer::NextScene() {
	//switcing to a scene for the first time while frozen causes certain things to not be initialised
	freeze = false;
	currentScene++;
	if (currentScene >= SCENENUMBER) {
		currentScene = 0;
	}
}
void Renderer::PreviousScene() {
	//switcing to a scene for the first time while frozen causes certain things to not be initialised
	freeze = false;
	//if the current scene is zero set it to the number so it becomes the last scene when 1 is subtracted
	if (currentScene == 0) {
		currentScene = SCENENUMBER;
	}
	currentScene--;
}

void Renderer::DrawSkybox() {
	//turn off cull face
	glDisable(GL_CULL_FACE);
	glDepthMask(GL_FALSE);
	glUseProgram(skyboxShader->GetProgram());
	UpdateShaderMatrices(skyboxShader->GetProgram());
	//cube map is texture 1
	glUniform1i(glGetUniformLocation(skyboxShader->GetProgram(), "cubeTex"), 1);
	//bind the correct cubemap to the buffer
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_CUBE_MAP, skyBox[currentScene]);
	quad->Draw();
	glEnable(GL_CULL_FACE);
	glDepthMask(GL_TRUE);
}


void Renderer::ResetKnights() {
	nextKnight = 0;
	for (int i = 0; i < KNIGHT_NUMBER; i++) {
		Vector3 pos;
		while (pos.y < 100) {
			pos = heightMap3->GetPoint();
		}
		knights3[i]->SetTransform(Matrix4::Translation(pos));
		knights3[i]->StopFlying();
	}
}