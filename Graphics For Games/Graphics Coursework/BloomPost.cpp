#include "BloomPost.h"

BloomPost::BloomPost(Shader* combineShader, Shader* blurShader, Shader* lightShader, Mesh* quad,int width, int height, int it):
PostProcessor(combineShader, quad),
iterations(it),
blurShader(blurShader),
lightShader(lightShader),
width(width),
height(height){
	//make colour buffer
	glGenTextures(1, &lightBuffer);
	glBindTexture(GL_TEXTURE_2D, lightBuffer);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0,
		GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	//generate frame buffer
	glGenFramebuffers(1, &lightFrameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, lightFrameBuffer);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
		GL_TEXTURE_2D, lightBuffer, 0);
}

BloomPost::~BloomPost() {
	glDeleteTextures(1, &lightBuffer);
	glDeleteFramebuffers(1, &lightFrameBuffer);
}

void BloomPost::Draw(GLuint buffer, GLuint from, GLuint to) {
	if (on) {
		glUseProgram(lightShader->GetProgram());

		UpdateMatrices(lightShader);
		//
		glBindFramebuffer(GL_FRAMEBUFFER, lightFrameBuffer);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, lightBuffer, 0);
		quad->SetTexture(from);
		quad->Draw();

		glUseProgram(blurShader->GetProgram());
		glBindFramebuffer(GL_FRAMEBUFFER, lightFrameBuffer);
		UpdateMatrices(blurShader);
		glUniform2fv(glGetUniformLocation(blurShader->GetProgram(), "pixelSize"), 1, (float*)&Vector2((float)1 / width, (float)1 / height));
		//blur the texture
		for (int i = 0; i < iterations; i++) {
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, to, 0);
			glUniform1i(glGetUniformLocation(blurShader->GetProgram(), "isVertical"), true);
			quad->SetTexture(lightBuffer);
			quad->Draw();

			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, lightBuffer, 0);
			glUniform1i(glGetUniformLocation(blurShader->GetProgram(), "isVertical"), false);
			quad->SetTexture(to);
			quad->Draw();
		}

		glUseProgram(shader->GetProgram());
		glBindFramebuffer(GL_FRAMEBUFFER, buffer);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, to, 0);
		UpdateMatrices(shader);
		//put the light buffer texture on
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, lightBuffer);
		glUniform1i(glGetUniformLocation(shader->GetProgram(), "bloomTex"), 1);
		quad->SetTexture(from);
		quad->Draw();

		next->Draw(buffer, to, from);
	}
	else {
		next->Draw(buffer, from, to);
	}
}