#include "TextDisplay.h"

TextDisplay::TextDisplay(Mesh* quad, Font* font, std::string text, float x, float y):
font(font),
text(text),
x(x),
y(y)
{
	this->quad = quad;
	shader = new Shader(SHADERDIR"TexturedVertex.glsl", SHADERDIR"TexturedFragment.glsl");
	if (!shader->LinkProgram()) {
		return;
	}
}


TextDisplay::~TextDisplay(void) {}

void TextDisplay::Draw(GLuint buffer, GLuint from, GLuint to) {
	if (SetUp(buffer, from, to)) {

		//draw background since this is pretending to be a post processor
		quad->SetTexture(from);
		quad->Draw();
		TextMesh* textMesh = new TextMesh(text, *font);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE);
		//drawing text stuff goes here
		//update model matrix
		Matrix4 modelMatrix = Matrix4::Translation(Vector3(x -1, y -1, 0)) * Matrix4::Scale(Vector3(0.04, 0.05, 1))
		* Matrix4::Rotation(180, Vector3(1,0,0));
		glUniformMatrix4fv(glGetUniformLocation(shader->GetProgram(), "modelMatrix"), 1, false, (float*)&modelMatrix);
		textMesh->Draw();
		delete textMesh;
		glDisable(GL_BLEND);
		//if next exists 
		if (next) {
			next->Draw(buffer, to, from);
		}
	}
	else {
		next->Draw(buffer, from, to);
	}
	glUseProgram(0);
}