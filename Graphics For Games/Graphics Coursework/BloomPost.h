#pragma once
#ifndef BLOOMPOST_H
#define BLOOMPOST_H

#include "PostProcessor.h"

class BloomPost: public PostProcessor {
public:
	BloomPost(Shader* combineShader, Shader* blurShader, Shader* lightShader, Mesh* quad, int width, int height, int it = 10);
	virtual ~BloomPost(void);

	void Draw(GLuint buffer, GLuint from, GLuint to);
protected:
	//the buffer for the light
	GLuint lightFrameBuffer;
	GLuint lightBuffer;
	Shader* blurShader;
	Shader* lightShader;
	int iterations;
	int width;
	int height;
};

#endif