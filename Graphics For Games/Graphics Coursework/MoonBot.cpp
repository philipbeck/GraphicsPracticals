#include "MoonBot.h"

MoonBot::MoonBot(Mesh*m, HeightMap* heightMap, int startX, int startY):
SceneNode(m),
heightMap(heightMap),
x(startX),
y(startY){
	time = 0;
	speed = 100;
	//set direction randomly
	ChangeDir();
	//set the starting transform
	transform = Matrix4::Translation(heightMap->GetPoint(x, y)) *
		Matrix4::Rotation(180, heightMap->GetNormal(x, y));
}


void MoonBot::Update(float ms) {
	time += ms;
	if (time > speed) {
		time = 0;
		x += xDir;
		y += yDir;
		//sometimes change direction or if an edge is reached randomly
		//lots of ifs since something slightly different must happen
		if (x < 0) {
			x++;
			ChangeDir();
		}
		else if (x >= RAW_HEIGHT) {
			x--;
			ChangeDir();
		}
		else if (y < 0) {
			y++;
			ChangeDir();
		}
		else if (y >= RAW_HEIGHT) {
			y--;
			ChangeDir();
		}
		else if (rand() % 50 == 0) {
			ChangeDir();
		}
		//set the height and angle
		SetTransform(Matrix4::Translation(heightMap->GetPoint(x, y)) *
			Matrix4::Rotation(180, heightMap->GetNormal(x, y)));
	}
	SceneNode::Update(ms);
}

void MoonBot::ChangeDir() {
	//just set xDir and yDir to a value between 1 and -1
	//(this sometimes means a robot wont move
	xDir = (rand() % 3) - 1;
	yDir = (rand() % 3) - 1;
}