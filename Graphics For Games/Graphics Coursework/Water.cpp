#include "Water.h"

Water::Water(Mesh* quad, GLuint *cubemap):
SceneNode(quad){
	this->cubemap = cubemap;
	textureMatrix = Matrix4::Scale(Vector3(2.5, 3, 3));
}

void Water::Update(float ms) {
	SceneNode::Update(ms);
	//change height eventually
	textureMatrix = textureMatrix * Matrix4::Rotation(ms/2000, Vector3(0, 0, 1));
}

void Water::Draw(const OGLRenderer &r) {

	if (mesh) {	
		glUniformMatrix4fv(glGetUniformLocation(shader, "textureMatrix"), 1, false, (float*)&textureMatrix);
		if (cubemap) {
			//make the water slightly transparent if it is reflective
			glEnable(GL_BLEND);
			glUniform1i(glGetUniformLocation(shader, "cubeTex"), 2);
			glActiveTexture(GL_TEXTURE2);
			glBindTexture(GL_TEXTURE_CUBE_MAP, *cubemap);
		}
		mesh->Draw();
	}
	glDisable(GL_BLEND);
}