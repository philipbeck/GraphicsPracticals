#pragma once
#ifndef SCREENNODE_H
#define SCREENNODE_H

#include "../../nclgl/SceneNode.h"

class ScreenNode :public SceneNode {
public:
	ScreenNode();
	virtual ~ScreenNode(void);

	virtual void Draw();
protected:

};

#endif