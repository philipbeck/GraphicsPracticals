#pragma once
#ifndef LIGHTNING_H
#define LIGHTNING_H

#include "../../nclgl/SceneNode.h"


class Lightning :public SceneNode {
public:
	Lightning();
	virtual ~Lightning(void) {};

	inline static void MakeLightning() { lightning = Mesh::GenerateLine(); }
	inline static void DeleteLightning() {
		if (lightning) { 
			delete lightning;
			lightning = NULL;
		}
	}

	virtual void Update(float msec);
	virtual void Draw(const OGLRenderer &r);
	//see if the lightening is already stricking
	inline bool GetStriking() { return striking; }
	//starts the lightening strike
	void Strike();
protected:
	static Mesh* lightning;
	//random number
	int r;
	float completion;
	bool striking;
};

#endif