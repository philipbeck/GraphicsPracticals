#include "PerPixelPost.h"

//constructor
PerPixelPost::PerPixelPost(Shader* s, Mesh* quad, float  pixelWidth, float pixelHeight) :
	PostProcessor(s, quad),
	pixelWidth(pixelWidth),
	pixelHeight(pixelHeight)
{}

//draw
void PerPixelPost::Draw(GLuint buffer, GLuint from, GLuint to) {
	if (SetUp(buffer, from, to)) {
		glUniform2f(glGetUniformLocation(shader->GetProgram(), "pixelSize"), pixelWidth, pixelHeight);

		quad->SetTexture(from);
		quad->Draw();
		//if next exists 
		if (next) {
			next->Draw(buffer, to, from);
		}
	}
	else {
		next->Draw(buffer, from, to);
	}
	glUseProgram(0);
}