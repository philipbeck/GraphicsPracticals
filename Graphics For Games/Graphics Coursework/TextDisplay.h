#pragma once
#ifndef TEXTDISPLAY_H
#define TEXTDISPLAY_H

#include "TextMesh.h"
#include "PostProcessor.h"

//not really a postprocessor but goes in the same place
class TextDisplay:public PostProcessor {
public:
	TextDisplay(Mesh* quad, Font* font, std::string text = "", float x = 0, float y = 0);
	virtual ~TextDisplay(void);

	void Draw(GLuint buffer, GLuint from, GLuint to);

	inline void Update(std::string text) { this->text = text; }
protected:
	Font* font;
	std::string text;
	float x,y;
};

#endif