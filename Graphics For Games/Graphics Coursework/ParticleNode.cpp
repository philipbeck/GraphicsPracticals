#include "ParticleNode.h"

ParticleNode::ParticleNode(ParticleEmitter* emitter, GLuint shader):
//include emitter so the rest of the program doesn't complain
SceneNode(emitter),
emitter(emitter){
	this->shader = shader;
}

void ParticleNode::Update(float ms) {
	emitter->Update(ms);
}

void ParticleNode::Draw(const OGLRenderer &r) {
	glUseProgram(shader);
	glUniform1f(glGetUniformLocation(shader, "particleSize"), emitter->GetParticleSize());
	glUniformMatrix4fv(glGetUniformLocation(shader, "modelMatrix"), 1, false, (float*)&transform);
	emitter->Draw();
}