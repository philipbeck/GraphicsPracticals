#pragma once
#ifndef MOONBOT_H
#define MOONBOT_H

#include "../../nclgl/HeightMap.h"
#include "../../nclgl/SceneNode.h"

class MoonBot :public SceneNode {
public:
	MoonBot(Mesh*m, HeightMap* heightMap,int startX,int startY);
	virtual ~MoonBot(void) {};

	void Update(float ms);
	//leave draw the same
protected:
	void ChangeDir();

	HeightMap* heightMap;

	int x;
	int y;
	int xDir;
	int yDir;
	float time;
	//how often the robot moves in it's give direction
	float speed;
};

#endif