#include "PostProcessor.h"

PostProcessor::PostProcessor(Shader* s, Mesh* quad): shader(s), quad(quad) {
	next = NULL;
	on = true;
}

PostProcessor::~PostProcessor() {
	delete shader;
	delete next;
}

//getters and setters
void PostProcessor::PushEnd(PostProcessor* p) {
	if (next) {
		next->PushEnd(p);
	}
	else {
		next = p;
	}
}

void PostProcessor::PutNext(PostProcessor* p) {
	//use push end incase p is a chain of prost processors
	p->PushEnd(next);
	//make next p
	next = p;
}


bool PostProcessor::SetUp(GLuint buffer, GLuint from, GLuint to) {
	if (!on) {
		return false;
	}

	if (next) {
		glBindFramebuffer(GL_FRAMEBUFFER, buffer);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, to, 0);
	}
	else {
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	glUseProgram(shader->GetProgram());

	//clear the buffer
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	UpdateMatrices(shader);

	return true;
}

void PostProcessor::UpdateMatrices(Shader* shader) {
	Matrix4 projMatrix = Matrix4::Orthographic(-1, 1, 1, -1, -1, 1);
	//all of these are just the identity
	Matrix4 viewMatrix;
	Matrix4 modelMatrix;
	Matrix4 textureMatrix;
	glUniformMatrix4fv(glGetUniformLocation(shader->GetProgram(), "modelMatrix"), 1, false, (float*)&modelMatrix);
	glUniformMatrix4fv(glGetUniformLocation(shader->GetProgram(), "viewMatrix"), 1, false, (float*)&viewMatrix);
	glUniformMatrix4fv(glGetUniformLocation(shader->GetProgram(), "projMatrix"), 1, false, (float*)&projMatrix);
	glUniformMatrix4fv(glGetUniformLocation(shader->GetProgram(), "textureMatrix"), 1, false, (float*)&textureMatrix);
}

//DOUBLE VISION STUFF
DoubleVisionPost::DoubleVisionPost(Mesh* quad) {
	this->quad = quad;

	shader = new Shader(SHADERDIR"TexturedVertex.glsl", SHADERDIR"DoubleFrag.glsl");
	if (!shader->LinkProgram()) {
		return;
	}
	next = NULL;
	on = true;
	time = 0.0;
}

void DoubleVisionPost::Update(float msec) {
	time += msec;
}

void DoubleVisionPost::Draw(GLuint buffer, GLuint from, GLuint to) {
	if (SetUp(buffer, from, to)) {

		glUniform1i(glGetUniformLocation(shader->GetProgram(), "time"), time);

		quad->SetTexture(from);
		quad->Draw();
		//if next exists 
		if (next) {
			next->Draw(buffer, to, from);
		}
	}
	else {
		next->Draw(buffer, from, to);
	}
	glUseProgram(0);
}