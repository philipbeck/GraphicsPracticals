#pragma once
#ifndef POSTPROCESSOR_H
#define POSTPROCESSOR_H

#include "../../nclgl/Shader.h"

class PostProcessor {
public:
	PostProcessor():next(NULL) {}
	PostProcessor(Shader*, Mesh* quad);
	virtual ~PostProcessor();
	//draw effects
	virtual void Draw(GLuint buffer, GLuint from, GLuint to) = 0;
	//adding post processor effects
	//put the effect at the end
	void PushEnd(PostProcessor*);
	void PutNext(PostProcessor*);
	PostProcessor* GetNext() { return next; }
	//setting up the stuff all post processors will use
	bool SetUp(GLuint buffer, GLuint from, GLuint to);
	void UpdateMatrices(Shader* shader);

	inline void ToggleOn() { on = !on; }
protected:
	Mesh* quad;
	Shader* shader;
	PostProcessor* next;
	//so the post processor can be turned on or off easily
	bool on;
};

class DoubleVisionPost : public PostProcessor {
public:
	DoubleVisionPost(Mesh* quad);
	virtual ~DoubleVisionPost() {};

	void Draw(GLuint buffer, GLuint from, GLuint to);

	void Update(float msec);
protected:
	float time;
};

#endif