#include "Lightning.h"

Mesh* Lightning::lightning;

//constructor
Lightning::Lightning() : SceneNode(lightning) {
	mesh = lightning;
	striking = false;
}

//destructor


void Lightning::Draw(const OGLRenderer &renderer) {
	if (striking && mesh) {
		glUniform1i(glGetUniformLocation(shader, "rand"), r);
		glUniform1f(glGetUniformLocation(shader, "completion"), completion);
		mesh->Draw();
	}
}

void Lightning::Update(float msec) {
	SceneNode::Update(msec);
	//200 is just a magic number for now
	if (striking && completion < 200) {
		completion += msec;
	}
	else {
		striking = false;
	}
}


void Lightning::Strike() {
	//don't do anything if the lightening is already striking
	if (striking) {
		return;
	}
	//make a new random number
	r = rand();
	completion = 0;
	striking = true;
}