#pragma once
#ifndef PARTICLENODE_H
#define PARTICLENODE_H

#include "../../nclgl/SceneNode.h"
#include "ParticleEmitter.h"

class ParticleNode:public SceneNode {
public:
	ParticleNode(ParticleEmitter* emitter, GLuint shader);
	virtual ~ParticleNode(void) {};

	virtual void Update(float msec);
	virtual void Draw(const OGLRenderer &r);
protected:
	ParticleEmitter* emitter;
};

#endif