#pragma comment(lib, "nclgl.lib")

//for seeding random number generator
#include <time.h>

#include "../../NCLGL/window.h"
#include "Renderer.h"

int main() {
	//seed the random number generator first!
	srand(time(NULL));

	Window w("Graphics Coursework", 1920, 1080, true); //This is all boring win32 window creation stuff!
	//Window w("Graphics Coursework", 1200, 800, false);//for when I have to look at the console
	if (!w.HasInitialised()) {
		return -1;
	}


	Renderer renderer(w); //This handles all the boring OGL 3.2 initialisation stuff, and sets up our tutorial!
	if (!renderer.HasInitialised()) {
		return -1;
	}

	w.LockMouseToWindow(true);
	w.ShowOSPointer(false);

	while (w.UpdateWindow() && !Window::GetKeyboard()->KeyDown(KEYBOARD_ESCAPE)) {
		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_F)) {
			renderer.ToggleFreeze();
		}
		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_PLUS)) {
			renderer.SetSpeed(renderer.GetSpeed() + 0.1f);
		}
		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_MINUS) && renderer.GetSpeed() > 0.2) {
			renderer.SetSpeed(renderer.GetSpeed() - 0.1f);
		}
		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_0)) {
			//reset the speed
			renderer.SetSpeed(1.0);
		}
		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_LEFT)) {
			renderer.PreviousScene();
		}
		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_RIGHT)) {
			renderer.NextScene();
		}
		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_1)) {
			renderer.ToggleCRT();
		}
		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_2)) {
			renderer.ToggleDouble();
		}
		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_3)) {
			renderer.ToggleBloom();
		}
		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_PAUSE)) {
			renderer.ToggleAutoCycle();
		}
		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_R)) {
			renderer.ResetKnights();
		}
		renderer.UpdateScene(w.GetTimer()->GetTimedMS());
		renderer.RenderScene();
	}

	return 0;
}