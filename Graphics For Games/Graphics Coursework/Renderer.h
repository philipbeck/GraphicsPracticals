#pragma once
#ifndef RENDERER_H
#define RENDERER_H

#include "../../nclgl/OGLRenderer.h"
#include "../../nclgl/Camera.h"
#include "../../nclgl/SceneNode.h"
#include "../../nclgl/HeightMap.h"
#include "../../nclgl/MD5Node.h"
#include "../../nclgl/MD5Mesh.h"
#include "../../nclgl/Frustum.h"
#include "../../nclgl/OBJMesh.h"

#include "Lightning.h"
#include "PostProcessor.h"
#include "PerPixelPost.h"
#include "BloomPost.h"
#include "TextDisplay.h"
#include "World.h"
#include "Water.h"
#include "ParticleEmitter.h"
#include "ParticleNode.h"
#include "MoonBot.h"

#define LIGHTINGBOLTS 10
#define SCENENUMBER 3
#define KNIGHT_NUMBER 40
#define MOONBOT_NUMBER 7

class Renderer: public OGLRenderer {
public:
	Renderer(Window &parent);
	virtual ~Renderer(void);

	void UpdateScene(float ms);
	void RenderScene();

	inline float GetFov() const { return fov; }
	inline void SetFov(float f) { fov = f; }

	inline void ToggleAutoCycle() { autoCycle = !autoCycle; }
	inline void ToggleFreeze() { freeze = !freeze; }
	inline void SetSpeed(float speed) { this->speed = speed; }
	inline float GetSpeed() { return speed; };

	//turning on and off post processes
	inline void ToggleDouble() { doublePost->ToggleOn(); }
	inline void ToggleCRT() { crtPost->ToggleOn(); }
	inline void ToggleBloom() { bloomPost->ToggleOn(); }

	//what happens when someone left clicks
	void ResetKnights();

	void NextScene();
	void PreviousScene();

	void Strike(int n);
protected:
	//update shader matrices for a specific shader
	void UpdateShaderMatrices(GLuint program);
	//for drawing scenes
	void SwitchToPerspective();
	void SetShaderLight(GLuint shader,const Light& l);
	void DrawNode(SceneNode*);
	//drawing a skybox
	void DrawSkybox();

	//stuff for all scenes************************************************************
	//for post processing and the occasions other job which might want to use a quad
	Mesh* quad;
	PostProcessor* postProcessors;
	DoubleVisionPost* doublePost;
	PerPixelPost* crtPost;
	BloomPost* bloomPost;
	TextDisplay* infoDisplay;
	//font stuff
	Font* font;
	//camera for all scenes
	Camera* camera;
	//frustum for frustum culling
	Frustum frameFrustum;	//hell knight data
	MD5FileData* knightData;
	//scene node stuff
	SceneNode* scenes[3];
	int currentScene;
	//cubemaps for the skys
	GLuint skyBox[3];
	//scene 1  111111111111111111111111111111111111111111111111111111111111111111111111111
	HeightMap* heightMap1;
	Light* light;
	Lightning* lightning[LIGHTINGBOLTS];
	MD5Node* knight;
	Mesh* waterQuad;//lava and water have thier own quads until I add textures into scene nodes
	//water node
	Water* water;
	//scene 222222222222222222222222222222222222222222222222222222222222222222222222222222
	HeightMap* heightMap2;
	WorldNode* worldNode;
	OBJMesh* robot;
	MoonBot* moonBots[MOONBOT_NUMBER];
	//scene 3333333333333333333333333333333333333333333333333333333333333333333333333333333
	HeightMap* heightMap3;
	MD5Node* knights3[KNIGHT_NUMBER];
	Light* lights3;
	int nextKnight;
	Mesh* lavaQuad;
	//Lava is a water! (exept this one doesn't reflect)
	Water* lava;
	//partical emitter for sparks
	ParticleEmitter* emitter;
	//Shaders
	Shader* shadowShader;
	Shader* sceneShader;
	Shader* lightningShader;
	Shader* bumpShader;
	//poorly named shader for rendering multiple lights instead of just one!
	Shader* bumpShader2;
	Shader* knightShader;
	Shader* skyboxShader;
	Shader* waterShader;
	Shader* particleShader;
	//Program variables
	float time;
	float completion;
	int random;
	float fov;
	bool freeze; //this will stop the scene being updated if true
	float speed;
	//for auto cycling round the program
	bool autoCycle;
	float changeRate;//how long a scene will run for before it changes automatically
	float sceneTime;//how long this scene has run for
	//buffers and friends
	GLuint bufferFBO;
	GLuint processFBO;
	GLuint colourBuffers[2];
	GLuint bufferDepthTex;
	//final buffer for the scene nodes to draw into if there are post processors
	GLuint finalBuffer;
};

#endif