#include "World.h"

WorldNode::WorldNode(GLsizei& width, GLsizei& height, Mesh* mesh, Vector4 colour):
SceneNode(mesh, colour),
width(&width),
height(&height){
	sun = new Light(Vector3(-100,1500,-100), Vector4(1, 1, 1, 1), 7000.0);
	shadowShader = NULL;

	glGenTextures(1, &shadowTex);
	glBindTexture(GL_TEXTURE_2D, shadowTex);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT,
		SHADOW_SIZE, SHADOW_SIZE, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);

	glBindTexture(GL_TEXTURE_2D, 0);

	glGenFramebuffers(1, &shadowFBO);

	glBindFramebuffer(GL_FRAMEBUFFER, shadowFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
		GL_TEXTURE_2D, shadowTex, 0);

	glDrawBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

WorldNode::~WorldNode(void) {
	delete sun;
	//delete frame buffers
	glDeleteTextures(1, &shadowTex);
	glDeleteFramebuffers(1, &shadowFBO);
}

void WorldNode::Draw(const OGLRenderer &r) {
	DrawShadowScene();
	DrawCombinedScene();
}

void WorldNode::DrawShadowScene() {
	glBindFramebuffer(GL_FRAMEBUFFER, shadowFBO);

	glClear(GL_DEPTH_BUFFER_BIT);

	glViewport(0, 0, SHADOW_SIZE, SHADOW_SIZE);

	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

	glUseProgram(shadowShader);

	//stays at Identity
	Matrix4 modelMatrix;
	Matrix4 projMatrix = Matrix4::Perspective(0.1, 10000, 1, 80);
	Matrix4 viewMatrix = Matrix4::BuildViewMatrix(sun->GetPosition(), Vector3(1500, 0, 1500));
	Matrix4 textureMatrix = biasMatrix*(projMatrix*viewMatrix);

	glUniformMatrix4fv(glGetUniformLocation(shadowShader, "modelMatrix"), 1, false, (float*)&modelMatrix);
	glUniformMatrix4fv(glGetUniformLocation(shadowShader, "viewMatrix"), 1, false, (float*)&viewMatrix);
	glUniformMatrix4fv(glGetUniformLocation(shadowShader, "projMatrix"), 1, false, (float*)&projMatrix);//(float*)&projMatrix);
	glUniformMatrix4fv(glGetUniformLocation(shadowShader, "textureMatrix"), 1, false, (float*)&textureMatrix);

	mesh->Draw();

	//go through and draw everything in it's nodes on the same kind of way the normal scene does
	//for now just do the first layer
	for (vector<SceneNode*>::const_iterator i = GetChildIteratorStart();
		i != GetChildIteratorEnd(); i++) {
		modelMatrix = (*i)->GetTransform() * Matrix4::Scale((*i)->GetModelScale());
		glUniformMatrix4fv(glGetUniformLocation(shadowShader, "modelMatrix"), 1, false, modelMatrix.values);
		glUniformMatrix4fv(glGetUniformLocation(shadowShader, "textureMatrix"),
			1, false, (textureMatrix*modelMatrix).values);
		(*i)->mesh->Draw();
	}

	glUseProgram(0);
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	//turn the view matrix back to normal
	glViewport(0, 0, *width, *height);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void WorldNode::DrawCombinedScene() {
	glBindFramebuffer(GL_FRAMEBUFFER, *finalBuffer);
	glUseProgram(shader);
	//textures
	glUniform1i(glGetUniformLocation(shader, "diffuseTex"), 0);
	glUniform1i(glGetUniformLocation(shader, "bumpTex"), 1);
	glUniform1i(glGetUniformLocation(shader, "shadowTex"), 2);
	//stuff for the sun
	glUniform3fv(glGetUniformLocation(shader,
		"lightPos"), 1, (float*)&sun->GetPosition());
	glUniform4fv(glGetUniformLocation(shader,
		"lightColour"), 1, (float*)&sun->GetColour());
	glUniform1f(glGetUniformLocation(shader,
		"lightRadius"), sun->GetRadius());

	Matrix4 projMatrix = Matrix4::Perspective(0.1, 12000, 1, 80);
	Matrix4 viewMatrix = Matrix4::BuildViewMatrix(sun->GetPosition(), Vector3(1500, 0, 1500));
	Matrix4 textureMatrix = biasMatrix*(projMatrix*viewMatrix);
	Matrix4 modelMatrix = GetWorldTransform();
	Matrix4 tempMatrix = textureMatrix * modelMatrix;
	glUniformMatrix4fv(glGetUniformLocation(shader, "textureMatrix"),
		1, false, tempMatrix.values);
	glUniformMatrix4fv(glGetUniformLocation(shader, "modelMatrix"),
		1, false, modelMatrix.values);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, shadowTex);

	mesh->Draw();

	glUseProgram(0);
}