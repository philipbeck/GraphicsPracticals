#pragma once
#ifndef PERPIXELPOST_H
#define PERPIXELPOST_H

#include "PostProcessor.h"

class PerPixelPost : public PostProcessor {
public:
	PerPixelPost(Shader*, Mesh* quad, float pixelWidth,float pixelHeight);
	virtual ~PerPixelPost(void) {};

	void Draw(GLuint buffer, GLuint from, GLuint to);
protected:
	float pixelWidth, pixelHeight;
};

#endif