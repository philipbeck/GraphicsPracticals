#pragma once
#ifndef WATER_H
#define WATER_H

#include "../../nclgl/SceneNode.h"
#include "../../nclgl/HeightMap.h"

class Water : public SceneNode {
public:
	Water(Mesh* quad, GLuint *cubemap = NULL);
	virtual ~Water(void) {};

	virtual void Update(float msec);
	virtual void Draw(const OGLRenderer &r);

	inline void SetTextureMatrix(Matrix4 m) { textureMatrix = m; }
protected:
	GLuint* cubemap;
	//wobbly water stuff
	GLuint waterLevelShader;
	GLuint waterBuffers[2];
	int current;
	Matrix4 textureMatrix;
};

#endif