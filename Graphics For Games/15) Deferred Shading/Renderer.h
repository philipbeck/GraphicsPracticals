#pragma once
#ifndef RENDERER_H
#define RENDERER_H

#include "../../nclgl/OGLRenderer.h"
#include "../../nclgl/Camera.h"
#include "../../nclgl/OBJMesh.h"
#include "../../nclgl/heightmap.h"

#define LIGHTNUM 8

class Renderer :public OGLRenderer {
public:
	Renderer(Window &parent);
	virtual ~Renderer(void);

	virtual void RenderScene();
	virtual void UpdateScene(float msec);
protected:
	void FillBuffers();
	void DrawPointLights();
	void CombineBuffers();
//make a new texture
	void GenerateScreenTexture(GLuint &into, bool depth = false);

	Shader *sceneShader;
	Shader *pointLightShader;
	Shader *combineShader;

	Light* pointLights;
	Mesh* heightMap;
	OBJMesh* sphere;
	Mesh* quad;
	Camera* camera;

	float rotation;

	GLuint bufferFBO;
	GLuint bufferColourTex;
	GLuint bufferNormalTex;
	GLuint bufferDepthTex;

	GLuint pointLightFBO;
	GLuint lightEmissiveTex;
	GLuint lightSpecularTex;
};

#endif