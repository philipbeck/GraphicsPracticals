#pragma once
#include "../../nclgl/OGLRenderer.h"

class Renderer : public OGLRenderer	{
public:
	Renderer(Window &parent);
	virtual ~Renderer(void);
	virtual void RenderScene();

protected:
	Mesh*	triangle;
	//extra triangle
	Mesh* triangle2;
	//differently made triangle
	Mesh* triangle3;
	Mesh* triangle4;
};
